import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


public class Server implements Runnable {
    private List<Client> clients = Collections.synchronizedList(new ArrayList<Client>());
    private int n;
    private AtomicInteger waitingPeriod;

    public Server(int n) {
        this.n = n;
        waitingPeriod = new AtomicInteger(0);
    }

    public void addClient(Client c) {
        this.clients.add(c);waitingPeriod.getAndAdd(c.getT_service());
    }
    public int getFinishTime() {
        return waitingPeriod.get();
    }
    public List<Client> getClients() {
        return clients;
    }
    public void run() {
        while (true) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (this.clients.isEmpty()!=true && this.clients.get(0).getT_service() != 0) {
                this.clients.get(0).setT_service(this.clients.get(0).getT_service() - 1);
                waitingPeriod.set(waitingPeriod.get()-1);
            }
            if (this.clients.isEmpty()!=true && this.clients.get(0).getT_service() <= 0) {
                this.clients.remove(0);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(waitingPeriod.get()==0){
                break;
            }

        }


    }
    public String getstatus() {
        String rez = new String("");
        if (this.clients.isEmpty())
            rez += ("Queue " + n + ": Closed\n");
        else {
            rez += ("Queue " + n + ": ");
            synchronized (clients) {
                for (Client c : this.getClients()) {
                    rez += ("(" + c.getID() + "," + c.getT_arrival() + "," + c.getT_service() + "); ");
                }
                rez += "\n";
            }

        }
        return rez;
    }



}
