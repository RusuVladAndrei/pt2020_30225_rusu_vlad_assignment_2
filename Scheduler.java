import java.util.*;


public class Scheduler {
    private List<Server> tails;
    private int maxnumber;private int maxtasks;private Strategy strategy;
    public Scheduler(int max1,int max2) {
        tails=new ArrayList<Server>();this.maxnumber=max1;this.maxtasks=max2;
        for(int i=0;i<maxnumber;i++) {
            Server s=new Server(i+1);
            tails.add(s);
        }
    }
    public void change(SPolicy policy) {
        if(policy==SPolicy.SHORTEST_TIME) {
            strategy=new ConStrat();
        }
    }

    public void giveTask(Client c) {
        strategy.addTask(tails, c);
    }

    public List<Server> getServers() {
        return tails;
    }

}
