import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Client{
    private int ID;
    private int t_arrival;
    private int t_service;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getT_arrival() {
        return t_arrival;
    }

    public void setT_arrival(int t_arrival) {
        this.t_arrival = t_arrival;
    }

    public int getT_service() {
        return t_service;
    }

    public void setT_service(int t_service) {
        this.t_service = t_service;
    }
    public List<Client> generate_clients(int n,int t_max_arr,int t_min_arr,int t_max_ser,int t_min_ser){
        int[] vec_id=new int[n];
        List<Client> rez=new ArrayList<>();int j=0;
        Random rand = new Random();
        int i=0;
        while(i!=n){
            Client c = new Client();
            int r1=0;
            while(true){
                r1=rand.nextInt(n)+1;
                if(vec_id[r1-1]==0){
                    vec_id[r1-1]=1;break;
                }
            }
            c.setID(r1);
            c.setT_arrival(rand.nextInt(t_max_arr - t_min_arr+1) + t_min_arr);
            c.setT_service(rand.nextInt(t_max_ser - t_min_ser+1) + t_min_ser);
            rez.add(c);
            i++;
        }
        return rez;
    }

}

