import java.util.List;
 interface Strategy {
    public void addTask(List<Server> servers,Client c);
}
public class ConStrat implements Strategy {
    public void addTask(List<Server> servers, Client c) {
        int min=servers.get(0).getFinishTime();
        for(int i=0;i<servers.size();i++) {
            if(servers.get(i).getClients().isEmpty()) {
                servers.get(i).addClient(c);
                Thread t=new Thread(servers.get(i));t.start();break;
            }
            else {
                for(int j=0;j<servers.size();j++) {
                    if(servers.get(j).getFinishTime()<min) {
                        min=servers.get(j).getFinishTime();
                    }
                }
                if(servers.get(i).getFinishTime()==min) {
                    servers.get(i).addClient(c);break;
                }
            }
        }

    }

}

