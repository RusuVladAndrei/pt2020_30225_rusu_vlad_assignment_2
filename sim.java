import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class sim implements Runnable {
    public int n;public int q;public int t_max;
    public int t_min_arrival;public int t_max_arrival;
    public int t_min_service;public int t_max_service;
    public double avg_time;
    public SPolicy policy = SPolicy.SHORTEST_TIME;
    public String to_print=new String("");
    public String in;
    public String out;

    private Scheduler scheduler;
    public List<Client> clienti;

    public sim(String in_file,String out_file) throws FileNotFoundException {
        this.in=in_file;this.out=out_file;
        File f = new File(in_file);File  f_out= new File(out_file);
        try {
            f_out.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scanner s = new Scanner(f);
        String a=new String();
        String[] str=new String[2];
        n = s.nextInt();
        q=s.nextInt();
        t_max=s.nextInt();
        s.nextLine();
        a=s.nextLine();
        str=a.split(",",2);
        t_min_arrival=Integer.parseInt(str[0]);
        t_max_arrival = Integer.parseInt(str[1]);
        a=s.nextLine();
        str=a.split(",");
        t_min_service = Integer.parseInt(str[0]);
        t_max_service= Integer.parseInt(str[1]);
        Client c=new Client();
        clienti=c.generate_clients(n, t_max_arrival, t_min_arrival, t_max_service, t_min_service).stream().collect(Collectors.toList());
        clienti.sort(Comparator.comparing(Client::getT_arrival));
        int sum=0;
        for(Client cc:clienti){
            sum+=cc.getT_service();
        }
        avg_time=(double)sum/n;
        scheduler = new Scheduler(q, 100);
        for(int i=0;i<q;i++) {
            Thread t=new Thread(scheduler.getServers().get(i));
            t.start();
        }
        scheduler.change(policy);

    }
    public void run() {
        int currentTime = 0;
        while (currentTime < t_max) {
            to_print+=("Time "+currentTime+"\n");
            for (ListIterator<Client> i = clienti.listIterator(); i.hasNext();) {
                Client c = i.next();
                if (c.getT_arrival() == currentTime ) {
                    scheduler.giveTask(c);
                    i.remove();
                }
            }
            to_print+=("Waiting Clients: ");
            for(Client c:clienti) {
                to_print += ("(" + c.getID() + "," + c.getT_arrival() + "," + c.getT_service() + "); ");
            }
            to_print+="\n";int suma=0;
            for(int i=0;i<q;i++) {
                to_print+=(scheduler.getServers().get(i).getstatus());
                suma+=scheduler.getServers().get(i).getFinishTime();
            }
            currentTime++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(clienti.isEmpty()){
                int p=0;
                for(int j=0;j<q;j++){
                    if(scheduler.getServers().get(j).getClients().isEmpty()){
                        p++;
                    }
                }
                if(p==q){
                    to_print+="\nAverage waiting time:" + avg_time;
                    break;
                }
            }
            if(currentTime==t_max){
                to_print+="\nMaximum time reached";
            }
        }

        try {
            FileWriter write = new FileWriter(out);
            write.write(to_print);
            write.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        sim gen=new sim(args[0],args[1]);
        Thread t=new Thread(gen);t.start();
    }
}

